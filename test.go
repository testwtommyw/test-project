package main

import (
    "fmt"
    "flag"
    "os"
    "bufio"
    "strconv"
)

func run_day_01_b() {
    fmt.Println("\n================\nDay 01 B\n================")

    filePtr := flag.String("fpathB", "input/day_01.txt", "File path to read from")
    startNumber := 0
    flag.Parse()
    fmt.Println("Input file:\t\t\t", *filePtr)
    fmt.Println("Starting frequency:\t\t", startNumber)

    file, err := os.Open(*filePtr)
    if(err != nil) {
        fmt.Println("Error opening file: ", err)
    }

    scanner := bufio.NewScanner(file)
    result := startNumber
    var pastFrequencies []int
    for scanner.Scan() {
      line := scanner.Text()
      operator := line[:1] // Get the first char of this line
      amount, _ := strconv.Atoi(line[1:]) // Get all the other chars of this line andconvert to int
      if(operator == "+") {
          result = result + amount
      } else {
          result = result - amount
      }

      if(isSeenBefore(result, pastFrequencies)) {
          fmt.Println(result, " has been seen before")
      }
      pastFrequencies = append(pastFrequencies, result)
      


    }
    file.Close()
    fmt.Println("Resulting requency:\t\t", result)

}

func isSeenBefore(frequency int, pastFrequencies []int) bool {
    for _, el := range pastFrequencies {
        if el == frequency {
            fmt.Println(el, "==", frequency)
            return true
        }
    }
    return false
}
